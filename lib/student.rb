class Student

  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    if (!@courses.include? new_course) && (!new_course.students.include? self)
      @courses << new_course
      new_course.students << self
    end
  end

  def course_load
    course_credit = Hash.new(0)

    @courses.each do |element|
      course_credit[element.department] += element.credits
    end

    course_credit
  end

end
